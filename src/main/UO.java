package main;

public class UO {

    public void MoP(int a, int b){
        if (a%2==0){
            System.out.println("'а' четное " + a*b);
        } else
        {
            System.out.println("'а' не четное " + a+b);
        }
    }

    public void wq(double x, double y){
        if (x<0) {
            if (y>0) {
                System.out.println("Точкк с координатами (" + x + ";" + y + ") принадлежит IV-й четверти");
            } else {
                System.out.println("Точкк с координатами (" + x + ";" + y + ") принадлежит III-й четверти");
            }
        }
        if (x>0) {
            if (y > 0) {
                System.out.println("Точкк с координатами (" + x + ";" + y + ") принадлежит I-й четверти");
            } else {
                System.out.println("Точкк с координатами (" + x + ";" + y + ") принадлежит II-й четверти");
            }
        }
    }

    public void sotp(double a, double b, double c){
        if (a>0 & b>0 & c>0){
            System.out.println("Сумма положительных чисел равна : " + (a+b+c));
        }
        if (a>0 & b>0 & c<0){
            System.out.println("Сумма положительных чисел равна : " + (a+b));
        }
        if (a<0 & b>0 & c>0){
            System.out.println("Сумма положительных чисел равна : " + (b+c));
        }
        if (a>0 & b<0 & c>0){
            System.out.println("Сумма положительных чисел равна : " + (a+c));
        }
        if (a<0 & b<0 & c<0){
            System.out.println("Нет положительных чисел");
        }
    }

    public void mm(double a, double b, double c){
        if ((a*b*c)>(a+b+c)){
            System.out.println("Произведение больше суммы, результат : " + (a*b*c+3));
        }
        if ((a*b*c)<(a+b+c)){
            System.out.println("Сумма больше произведения, результат : " + (a+b+c+3));
        }
    }

    public void rating(int a){
        if (0<=a & a<=19){
            System.out.println("Оценка студента : F");
        }
        if (20<=a & a<=39){
            System.out.println("Оценка студента : E");
        }
        if (40<=a & a<=59){
            System.out.println("Оценка студента : D");
        }
        if (60<=a & a<=74){
            System.out.println("Оценка студента : C");
        }
        if (75<=a & a<=89){
            System.out.println("Оценка студента : B");
        }
        if (90<=a & a<=100){
            System.out.println("Оценка студента : A");
        }
        if (a<0 | a>100){
            System.out.println("Рейтинг введен неверно");
        }
    }
}
