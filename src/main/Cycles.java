package main;

public class Cycles {
    public void sumAndQuantity(){

        int count=0, sum=0;

        for (int i = 1; i < 100; i++){
            if (i%2==0){
                count++;
                sum += i;
            }
        }
        System.out.println("Количество четных чисел отрезка [1;99] равна : " + count + ", а их сумма : " + sum);
    }

    public void isPrime(int a) {
        if (a == 2) {
            System.out.println("Число " + a + " простое");
        } else {
            if (a > 1) {
                if ((Math.pow(2, (a - 1)) - 1) % a == 0) {
                    System.out.println("Число " + a + " простое");
                } else {
                    System.out.println("Число " + a + " составное");
                }
            } else {
                System.out.println("Введено отрицательное число, один или ноль");
            }
        }
    }

    public int korenPodb(int x){
        int num = 1;
        int rezult = 0;

        while(x>0){
            x-=num;
            num+=2;
            rezult+=(x<0)?0:1;
        }
        return rezult;
    }

    public int dihotomy(int x){
        int a = 0, b = x, c;

        do {
            c = (a+b)/2;
            if (Math.pow(c,2)<=x){
                a = c;
            } else {
                b = c;
            }
        } while((b-a)>1);

        return c;
    }

    public int factorial(int x){
        int f=1;

        for (int i = 2; i < x; i++) {
            f*=i;
        }
        return f;
    }

    public int sumofnum(int x){
        int rez = 0;
        while (x != 0){
            rez += x % 10;
            x/=10;
        }
        return rez;
    }

    public StringBuffer reverse(int x){
        String inp = Integer.toString(x);
        StringBuffer buf = new StringBuffer();
        buf.setLength(inp.length());

        for (int i = 0; i < inp.length(); i++) {
            buf.setCharAt((inp.length()-1-i), inp.charAt(i));
        }

        return buf;
    }
}
