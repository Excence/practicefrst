package main;

public class Main {

    public static void main(String[] args) {
        UO uo = new UO();

        uo.MoP(4,3);
        uo.wq(-5.6, 4.7);
        uo.sotp(3, -7, 3);
        uo.mm(1, 4.5, -1);
        uo.rating(84);
        System.out.println("--------------------------------------------------------------------------------------------");
//----------------------------------------------------------------------------------------------------------------------
        Cycles cycles = new Cycles();

        cycles.sumAndQuantity();
        cycles.isPrime(41);
        System.out.println("Корень числа 27 методом подбора : " + cycles.korenPodb(27));
        System.out.println("Корень числа 27 методом дихотомии : " + cycles.dihotomy(27));
        System.out.println("Фвкториал числа 7 равен : " + cycles.factorial(7));
        System.out.println("Сумма цифр числа 1687787546 равна " + cycles.sumofnum(1687787546));
        System.out.println("Зеркальное отражение число 123456789 : " + cycles.reverse(123456789));
        System.out.println("--------------------------------------------------------------------------------------------");
// ---------------------------------------------------------------------------------------------------------------------
        Arrs arr = new Arrs();

        int[] arr1 = arr.create();
        int[] arrFR = arr1;
        int[] arrFB = arr1;
        int[] arrFS = arr1;
        int[] arrFI = arr1;
        int[] arrFQ = arr1;

        arr.vivod(arr1);

        System.out.println("Наименьший элемент массива "+arr.findMinEl(arr1));
        System.out.println("Наибольший элемент массива "+arr.findMaxEl(arr1));
        System.out.println("Индекс наименьшего элемента массива "+arr.findMinInd(arr1));
        System.out.println("Индекс наибольшего элемента массива "+arr.findMaxInd(arr1));
        System.out.println("Сумма элементов массива с нечетными индексами "+arr.sumOfOddInd(arr1));
        System.out.println("Реверс исходного массива");
        arr.vivod(arr.reverse(arrFR));
        System.out.println("Количество нечетных элементов массива "+arr.sumOfOddEl(arr1));
        System.out.println("Поменять половины массива местами");
        arr.vivod(arr.changeHalf(arr1));
        System.out.println("Сортировка массива пузырьком");
        arr.vivod(arr.sortBubble(arrFB));
        System.out.println("Сортировка массива выбором");
        arr.vivod(arr.sortSelect(arrFS));
        System.out.println("Сортировка массива вставками");
        arr.vivod(arr.sortInsert(arrFI));
        System.out.println("Быстрая сортировка массива");
        arr.vivod(arr.sortInsert(arrFQ));
    }
}
