package main;

public class Arrs {

    public int[] create(){

        int[] arr = new int[10];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = ((int)(Math.random() * 100) - 50);
        }
        return arr;
    }

    public void vivod(int[] inn){
        System.out.print("Массив :");
        for (int i = 0; i < inn.length; i++) {
            System.out.print(" "+inn[i]);
        }
        System.out.println();
    }

    public int findMinEl(int[] inn){
        int min = inn[0];
        for (int i = 0; i < inn.length; i++) {
            if (inn[i] < min){
                min = inn[i];
            }
        }
        return min;
    }

    public int findMaxEl(int[] inn){
        int max = inn[0];
        for (int i = 0; i < inn.length; i++) {
            if (inn[i] > max){
                max = inn[i];
            }
        }
        return max;
    }

    public int findMinInd(int[] inn){
        int rez = 0;
        int min = inn[0];
        for (int i = 0; i < inn.length; i++) {
            if (inn[i] < min){
                min = inn[i];
                rez = i+1;
            }
        }
        return rez;
    }

    public int findMaxInd(int[] inn){
        int rez = 0;
        int max = inn[0];
        for (int i = 0; i < inn.length; i++) {
            if (inn[i] > max){
                max = inn[i];
                rez = i+1;
            }
        }
        return rez;
    }

    public int sumOfOddInd(int[] inn){
        int rez=0;
        for (int i = 0; i < inn.length; i++) {
            rez += i%2 != 0 ? inn[i] : 0;
        }
        return rez;
    }

    public int[] reverse(int[] inn){
        int buf;

        for (int i = 0; i < inn.length/2; i++) {
            buf = inn[i];
            inn[i] = inn[inn.length-1-i];
            inn[inn.length-1-i] = buf;
        }
        return inn;
    }

    public int sumOfOddEl(int[] inn){
        int rez=0;
        for (int i = 0; i < inn.length; i++) {
            rez += inn[i]%2 != 0 ? 1 : 0;

        }
        return rez;
    }

    public int[] changeHalf(int[] inn){
        int buf;

        for (int i = 0; i < inn.length/2; i++) {
            buf = inn[i];
            inn[i] = inn[inn.length/2+i];
            inn[inn.length/2+i] = buf;
        }
        return inn;
    }

    public int[] sortBubble(int[] inn){
        int buf;

        for (int i = inn.length-1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (inn[j] > inn[j+1]){
                    buf = inn[j];
                    inn[j] = inn[j+1];
                    inn[j+1] = buf;
                }
            }

        }
        return inn;
    }

    public int[] sortSelect(int[] inn){
        int p;
        for (int i = 0; i < inn.length-1; i++) {
            p = i;
            for (int j = i+1; j < inn.length; j++) {
                if (inn[j] < inn[p]){
                    p = j;
                }
            }
            int buf = inn[i];
            inn[i] = inn[p];
            inn[p] = buf;
        }
        return inn;
    }

    public int[] sortInsert(int[] inn){
        int buf;
        for (int i = 0; i < inn.length; i++) {
            for (int j = i; j > 0 && inn[j-1]>inn[j]; j--) {
                buf = inn[j-1];
                inn[j-1] = inn[j];
                inn[j] = buf;
            }
        }
        return inn;
    }

    public int[] sortQuick(int[] inn, int frstInd, int lstInd){

        int i = frstInd, j = lstInd-1, midInd = (j-i)/2;
        double p = inn[midInd];

        do {
            while (inn[i]<=p) i++;
            while (inn[j]>=p) j--;

            if (i<=j){
                int buf = inn[i];
                inn[i] = inn[j];
                inn[j] = buf;
                i++; j--;
            }
        } while (i<=j);
        return inn;
    }
}